# build sops
FROM golang:1.18-rc-alpine3.15 AS builder

ARG SOPS_VERSION=v3.7.1
ENV SOPS_VERSION=$SOPS_VERSION

WORKDIR /go/src/go.mozilla.org/sops

RUN apk --no-cache add make \
    && wget https://github.com/mozilla/sops/archive/refs/tags/${SOPS_VERSION}.tar.gz \
    && tar -zxvf ${SOPS_VERSION}.tar.gz -C ./ \
    && mv ./sops*/* ./ \
    && CGO_ENABLED=1 make install

# build helm-deployer
FROM alpine:3.15

ARG KUBECTL_VERSION=v1.22.5
ARG HELM_VERSION=3.7.1
ARG HELM_SECRETS_VERSION=v3.12.0
ARG KUBEDOG_VERSION=v0.6.3
ARG DOCKER_VERSION=20.10.14

ENV KUBECTL_VERSION=$KUBECTL_VERSION
ENV HELM_VERSION=$HELM_VERSION
ENV HELM_SECRETS_VERSION=$HELM_SECRETS_VERSION
ENV HELM_HOME=/helm/
ENV YC_HOME=/yc
ENV DOCKER_VERSION=$DOCKER_VERSION

ENV PATH $HELM_HOME:$YC_HOME/bin:$PATH

RUN apk --no-cache add \
        curl \
        py-pip \
        py-crcmod \
        bash \
        libc6-compat \
        openssh-client \
        git \
        gnupg \
        docker \
        openrc \
        jq

# Copy sops

COPY --from=builder /go/bin/sops /usr/local/bin/sops

# Install KUBECTL
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

# Install HELM and HELM SECRETS
RUN curl -O https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar xzf helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64 $HELM_HOME && \
    rm helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    mkdir -p $HELM_HOME/plugins && \
    helm plugin install https://github.com/jkroepke/helm-secrets --version $HELM_SECRETS_VERSION

# Install Yandex Cloud tool YC
RUN curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | \
    bash -s -- -i ${YC_HOME} -n

RUN apk add --no-cache python3 py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install awscli \
    && rm -rf /var/cache/apk/*

# Install KUBEDOG
RUN curl -sSLO "https://tuf.kubedog.werf.io/targets/releases/0.6.3/linux-amd64/bin/kubedog" -O "https://tuf.kubedog.werf.io/targets/signatures/0.6.3/linux-amd64/bin/kubedog.sig" && \
    curl -sSL https://werf.io/kubedog.asc | gpg --import && \
    gpg --verify kubedog.sig kubedog && \
    mv kubedog /usr/local/bin/kubedog && \
    chmod +x /usr/local/bin/kubedog

VOLUME ["/root/.config"]
